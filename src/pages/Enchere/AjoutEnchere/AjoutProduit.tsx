import React, { useState, useRef ,useEffect} from "react";

import {
  IonToolbar,
  IonContent,
  IonPage,
  IonButtons,
  IonTitle,
  IonMenuButton,
  IonButton,
  IonIcon,
  IonHeader,
  IonCol,
  IonRow,
  IonItem,
  IonLabel,
  IonList,
  IonBackButton,
  IonTextarea,
  IonInput,
  IonSelect,
  IonSelectOption,
  
} from "@ionic/react";


import "./Enchere.scss";

import { connect } from "../../../data/connect";

import {  options, search } from "ionicons/icons";

// import { useParams } from "react-router";
import { Camera, CameraResultType, Photo } from "@capacitor/camera";

interface OwnProps {}

interface StateProps {
  mode: "ios" | "md";
}

interface DispatchProps {}

type AllPageProps = OwnProps & StateProps & DispatchProps;

const SchedulePage: React.FC<AllPageProps> = ({ mode }) => {
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const [image, setImage] = useState<Photo | undefined>(undefined);
  const designation = useRef(null);
  const description = useRef(null);
  const prixmin = useRef(null);
  const categ = useRef(null);
  const [categorie,setCategorie] = useState<any[]>([])

  useEffect(() => {
    fetch('https://limping-quince-production.up.railway.app/categorie')
    .then(response => response.json())
    .then(response => {
      console.log(response.data);
      setCategorie(response.data)
    })
  },[])

  const ios = mode === "ios";

  // const doRefresh = () => {
  //   setTimeout(() => {
  //     ionRefresherRef.current!.complete();
  //     setShowCompleteToast(true);
  //   }, 2500);
  // };

  // const idVoiture = useParams<{ id: string }>();

  const uploadImage = async () => {
    setImage(
      await Camera.getPhoto({
        quality: 90,
        allowEditing: true,
        resultType: CameraResultType.Base64,
      })
    );

    console.log(JSON.stringify(image));
  };
  let d = sessionStorage.getItem('token');
  if(d === null || d === undefined) {
    window.location.replace('/');
  }
  const token = JSON.parse(d !== null? d : '');
  const sendData = () => {
    let data = {
      designation: (designation?.current as unknown as HTMLInputElement)?.value,
      description: (description?.current as unknown as HTMLInputElement)?.value,
      prixmin: (prixmin?.current as unknown as HTMLInputElement)?.value,
      prixmax: 0,
      login: {
          id: token.login.id
      },
      photo: null,
      categorie: {
          id: (categ?.current as unknown as HTMLInputElement)?.value
      }
    }
    console.log(data)
    fetch('https://limping-quince-production.up.railway.app/product',{
      method : 'POST',
      headers : {
        'Content-Type': 'application/json'
      },
      body : JSON.stringify(data)
    }).then(response => response.json())
    .then(response => {
      sessionStorage.setItem('token',response.data);
      window.location.replace('/ListeProduit');
    })
    .catch(err => {

    });
  }

  return (
    <IonPage  id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/ListeEnchere"></IonBackButton>
          </IonButtons>
          {!showSearchbar && (
            <IonButtons slot="end">
              <IonMenuButton />
            </IonButtons>
          )}

          {!ios && !showSearchbar && <IonTitle>Ajouter Produits</IonTitle>}

          <IonButtons slot="end">
            {!ios && !showSearchbar && (
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            )}
            {!showSearchbar && (
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === "ios" ? (
                  "Filter"
                ) : (
                  <IonIcon icon={options} slot="icon-only" />
                )}
              </IonButton>
            )}
          </IonButtons>
        </IonToolbar>
      </IonHeader>
<br /><br /><br /><br />
      <IonContent fullscreen={true}>
      <form>
          <IonList>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Nom
              </IonLabel>
              <IonInput ref={designation} name="username" type="text" placeholder="Festival 2023"></IonInput>
            </IonItem>


            <IonItem>
              <IonLabel position="stacked" color="primary">
                Prix Minimum
              </IonLabel>
              <IonInput name="prenom" type="number" placeholder="12000" ref={prixmin}></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Categorie
              </IonLabel>
              <IonSelect placeholder="Select a value" ref={categ}>
                  {
                    categorie.map((cat) => (
                      <>
                      <IonSelectOption key={cat.id} value={cat.id}>{cat.designation}</IonSelectOption>
                      </>
                    ))
                  }
              </IonSelect>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Photo
              </IonLabel>
              <IonInput name="contact" type="text" placeholder="Add Path"></IonInput>
              <IonButton onClick= {uploadImage}>Ajouter</IonButton>
            </IonItem>
            <IonTextarea rows={3} autoGrow={true} placeholder="Description" ref={description} />
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton onClick={sendData} expand="block">
                Valider
              </IonButton>
            </IonCol>
          </IonRow>
        </form>
      </IonContent>
    </IonPage>
  );
};


export default connect<OwnProps, StateProps, DispatchProps>({
  mapDispatchToProps: {},
  component: React.memo(SchedulePage),
});
