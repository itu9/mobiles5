import React, { useState, useRef, useEffect } from "react";

import {
  IonToolbar,
  IonContent,
  IonPage,
  IonButtons,
  IonTitle,
  IonMenuButton,
  IonButton,
  IonIcon,
  IonHeader,
  IonCol,
  IonRow,
  IonItem,
  IonLabel,
  IonList,
  IonBackButton,
  IonInput,
} from "@ionic/react";

import "./Enchere.scss";

import { connect } from "../../../data/connect";

import { search } from "ionicons/icons";

// import { Camera, CameraResultType, Photo } from "@capacitor/camera";

interface OwnProps {}

interface StateProps {
  mode: "ios" | "md";
}

interface DispatchProps {}

type AllPageProps = OwnProps & StateProps & DispatchProps;

const SchedulePage: React.FC<AllPageProps> = ({ mode }) => {

  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  // const [showFilterModal, setShowFilterModal] = useState(false);
  // const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  // const [showCompleteToast, setShowCompleteToast] = useState(false);
  // const [image, setImage] = useState<Photo | undefined>(undefined);
  const pageRef = useRef<HTMLElement>(null);
  const title = useRef(null);
  const debut = useRef(null);
  const fin = useRef(null);
  const minimum = useRef(null);
  const prixMin = useRef(null);    
  const ios = mode === "ios";

  let prod = sessionStorage.getItem("produit");
  let token = sessionStorage.getItem("token");
  if(prod === null || prod === undefined) {
    window.location.replace('/ListeProduit')
    return <></>;
  }
  if(token === null || token === undefined) {
    window.location.replace('/')
    return <></>;
  }
  let produit = JSON.parse(prod);
  let login = JSON.parse(token).login
  const sendData = () => {
    let data = {
      designation : ((title?.current)  as unknown as HTMLInputElement).value,
      debut : ((debut?.current) as unknown as HTMLInputElement).value,
      fin : ((fin?.current) as unknown as HTMLInputElement).value,
      minimum : ((minimum?.current) as unknown as HTMLInputElement).value,
      prixMin : ((prixMin?.current) as unknown as HTMLInputElement).value,
      etat : 0,
      login : {
        id : login.id
      },
      produit : {
        id : produit.id
      }
    }
    console.log(data)
    // console.log(((fin?.current)  as unknown as HTMLInputElement).value);
    fetch('https://limping-quince-production.up.railway.app/enchere?idUser='+login.id,{
      method : 'POST',
      headers : {
        'Content-Type': 'application/json'
      },
      body : JSON.stringify(data)
    }).then(response => response.json())
    .then(response => {
      console.log(response)
      // window.location.replace('/ListeEnchere');
    })
    .catch(err => {

    });
  }

  // const doRefresh = () => {
  //   setTimeout(() => {
  //     ionRefresherRef.current!.complete();
  //     setShowCompleteToast(true);
  //   }, 2500);
  // };

  // const idVoiture = useParams<{ id: string }>();

  // const uploadImage = async () => {
  //   setImage(
  //     await Camera.getPhoto({
  //       quality: 90,
  //       allowEditing: true,
  //       resultType: CameraResultType.Base64,
  //     })
  //   );

  //   console.log(JSON.stringify(image));
  // };

  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/ListeEnchere"></IonBackButton>
          </IonButtons>
          {!showSearchbar && (
            <IonButtons slot="end">
              <IonMenuButton />
            </IonButtons>
          )}

          {!ios && !showSearchbar && <IonTitle>Ajouter Enchere</IonTitle>}

          <IonButtons slot="end">
            {!ios && !showSearchbar && (
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            )}
            {/* {!showSearchbar && (
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === "ios" ? (
                  "Filter"
                ) : (
                  <IonIcon icon={options} slot="icon-only" />
                )}
              </IonButton>
            )} */}
          </IonButtons>
        </IonToolbar>
      </IonHeader>
<br /><br /><br /><br />
      <IonContent fullscreen={true}>
      <form>
          <IonList>
            <IonItem>
              <h3>
                {produit.designation}
              </h3>
            </IonItem>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Titre Enchere
              </IonLabel>
              <IonInput name="username" type="text" placeholder="Festival 2023" ref={title}></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Prix de depart
              </IonLabel>
              <IonInput name="prenom" type="number" placeholder="12000" ref={prixMin}></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Minimum augmentation
              </IonLabel>
              <IonInput name="prenom" type="number" placeholder="12000" ref={minimum}></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Date Debut
              </IonLabel>
              <IonInput name="contact" type="datetime-local" placeholder="jj-mm-aa" ref={debut}></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Date Fin
              </IonLabel>
              <IonInput name="contact" type="datetime-local" placeholder="jj-mm-aa" ref={fin}></IonInput>
            </IonItem>
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton  expand="block" onClick={sendData}>
                Valider
              </IonButton>
            </IonCol>
          </IonRow>
        </form>
      </IonContent>
    </IonPage>
  );
};


export default connect<OwnProps, StateProps, DispatchProps>({
  mapDispatchToProps: {},
  component: React.memo(SchedulePage),
});
