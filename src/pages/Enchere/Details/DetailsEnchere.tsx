import React, { useState, useRef, useEffect } from "react";

import {
  IonToolbar,
  IonContent,
  IonPage,
  IonButtons,
  IonTitle,
  IonMenuButton,
  IonButton,
  IonIcon,
  IonHeader,
  IonItem,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonLabel,
  IonList,
  IonBackButton,
  IonTextarea
} from "@ionic/react";

import "./Enchere.scss";

import { connect } from "../../../data/connect";

import { logoChrome, options, search } from "ionicons/icons";

import { useParams } from "react-router";
import { SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG } from "constants";
import { Camera, CameraResultType, Photo } from "@capacitor/camera";
import endOfDay from "date-fns/esm/fp/endOfDay/index.js";

interface OwnProps {}

interface StateProps {
  mode: "ios" | "md";
}

interface DispatchProps {}

type AllPageProps = OwnProps & StateProps & DispatchProps;

const SchedulePage: React.FC<AllPageProps> = ({ mode }) => {
  const [segment, setSegment] = useState<"all" | "favorites">("all");
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  const [showCompleteToast, setShowCompleteToast] = useState(false);
  const [image, setImage] = useState<Photo | undefined>(undefined);
  const pageRef = useRef<HTMLElement>(null);
  const [enchere,setEnchere] = useState<any>(null);
  const [mise,setMise] = useState<any>(null);
  const [noMise,setNoMise] = useState<boolean>(true);
  const [h,setH] = useState<number>(0);
  const [m,setM] = useState<number>(0);
  const [s,setS] = useState<number>(0);
  const [clientMise, setClientMise] = useState<number>(0);


  const ios = mode === "ios";

  const doRefresh = () => {
    setTimeout(() => {
      ionRefresherRef.current!.complete();
      setShowCompleteToast(true);
    }, 2500);
  };

  const {id} = useParams<{ id: string }>();

  const uploadImage = async () => {
    setImage(
      await Camera.getPhoto({
        quality: 90,
        allowEditing: true,
        resultType: CameraResultType.Base64,
      })
    );

    console.log(JSON.stringify(image));
  };

  const [password, setPassword] = useState('motdepasse');
  const between = ( end : Date) => {
    let start = new Date(Date.now())
    let total = end.getTime() - start.getTime();
    let totalHours = Math.floor(total / (1000 * 60 * 60 * 24))
    let hours = totalHours;
    let minute = Math.floor((total / (1000 * 60)) % 60)
    let second = Math.floor((total / (1000 ))%60);
    setH(hours);
    setM(minute);
    setS(second);
  }

  useEffect(() => {
    fetch('https://limping-quince-production.up.railway.app/enchere/'+id)
    .then(response => response.json())
    .then(response => {
      console.log(response.data);
      setEnchere(response.data)
      let fin =new Date(Date.parse(response.data.fin))
      setInterval(() => {
        between(fin)
      } ,1000);
    })
    fetch('https://limping-quince-production.up.railway.app/mise/enchere/'+id)
    .then(response => response.json())
    .then(response => {

      if (response.code === undefined) {
        setMise(response.data)
        setNoMise(false);
      }
    })
  },[])



  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/ListeEnchere"></IonBackButton>
          </IonButtons>
          {!showSearchbar && (
            <IonButtons slot="end">
              <IonMenuButton />
            </IonButtons>
          )}

          {!ios && !showSearchbar && <IonTitle>Details Enchere</IonTitle>}

          <IonButtons slot="end">
            {!ios && !showSearchbar && (
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            )}
            {!showSearchbar && (
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === "ios" ? (
                  "Filter"
                ) : (
                  <IonIcon icon={options} slot="icon-only" />
                )}
              </IonButton>
            )}
          </IonButtons>
        </IonToolbar>
      </IonHeader>

       <IonContent fullscreen={true}>
        <IonCard className="speaker-card" >
          <IonCardHeader>
            <IonItem
              button
              detail={false}
              lines="none"
              className="speaker-item"
            >
              <IonLabel></IonLabel>
            </IonItem>
          </IonCardHeader>
          {
            enchere !== null ?
            <IonCardContent>
              <IonList lines="none">
                <IonLabel>
                  <IonItem>
                    <h1>{enchere.designation}</h1>
                  </IonItem>
                </IonLabel>
                <IonLabel>
                  <IonItem>
                  <img className="image" src="assets/img/appicon.svg" alt="Ionic logo" />
                  </IonItem>

                </IonLabel>


                <IonItem >
                <IonLabel>
                    <h3>
                      {enchere.produit.designation}
                      </h3>
                    {
                      noMise === false ?
                      <>
                        <h4>{mise.prix} ar</h4>
                        <p><span>{h}:{m}:{s}</span></p>
                          <input></input>
                          {/* <IonInput placeholder="Mettez votre mise" name="money" type="number" value={clientMise} onIonChange={e => setClientMise(parseInt(e.detail.value!))}  ></IonInput> */}
                        <IonButton>Encherir</IonButton>
                      </> : <IonLabel >L'enchere n'est pas en cours</IonLabel>
                    }
                    <br />
                    <h5>Date Debut</h5>
                    <p>{enchere.debut}</p>
                    <h5>Date Fin</h5>
                    <p>{enchere.fin}</p>
                    <h5>Prix Depart</h5>
                    <p>{enchere.minimum} ar</p>
                    <br/>
                    <IonTextarea rows={5} cols={6} value={enchere.produit.description} autoGrow={true} readonly  />
                    </IonLabel>
                </IonItem>
              </IonList>
            </IonCardContent> : <></>
          }
        </IonCard>
      </IonContent> 
    </IonPage>
  );
};
let textValue = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum quisquam sed expedita itaque dolores perspiciatis alias aliquid? Iure consectetur atque corrupti, laborum exercitationem nisi ipsum ut mollitia, quod consequatur optio. "


export default connect<OwnProps, StateProps, DispatchProps>({
  mapDispatchToProps: {},
  component: React.memo(SchedulePage),
});
