import React, { useState, useRef, useEffect } from "react";

import {
  IonToolbar,
  IonContent,
  IonPage,
  IonButtons,
  IonTitle,
  IonMenuButton,
  IonButton,
  IonIcon,
  IonHeader,
  IonItem,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonLabel,
  IonList,
  IonBackButton,
  IonTextarea,
} from "@ionic/react";

import "./Enchere.scss";

import { connect } from "../../../data/connect";

import {  options, search } from "ionicons/icons";

import { useParams } from "react-router";
// import { Camera, CameraResultType, Photo } from "@capacitor/camera";

interface OwnProps {}

interface StateProps {
  mode: "ios" | "md";
}

interface DispatchProps {}

type AllPageProps = OwnProps & StateProps & DispatchProps;

const SchedulePage: React.FC<AllPageProps> = ({ mode }) => {
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  // const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  // const [showCompleteToast, setShowCompleteToast] = useState(false);
  // const [image, setImage] = useState<Photo | undefined>(undefined);
  const pageRef = useRef<HTMLElement>(null);
  const [produits,setProduits] = useState<any>(null);

  const ios = mode === "ios";

  // const doRefresh = () => {
  //   setTimeout(() => {
  //     ionRefresherRef.current!.complete();
  //     setShowCompleteToast(true);
  //   }, 2500);
  // };

  const {id} = useParams<{ id: string }>();
  useEffect(() => {
    fetch('https://limping-quince-production.up.railway.app/product/'+id)
    .then(response => response.json())
    .then(response => {
      setProduits(response.data)
    })
  },[id])

  // const uploadImage = async () => {
  //   setImage(
  //     await Camera.getPhoto({
  //       quality: 90,
  //       allowEditing: true,
  //       resultType: CameraResultType.Base64,
  //     })
  //   );

  //   console.log(JSON.stringify(image));
  // };

  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/ListeEnchere"></IonBackButton>
          </IonButtons>
          {!showSearchbar && (
            <IonButtons slot="end">
              <IonMenuButton />
            </IonButtons>
          )}

          {!ios && !showSearchbar && <IonTitle>Details Produit</IonTitle>}

          <IonButtons slot="end">
            {!ios && !showSearchbar && (
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            )}
            {!showSearchbar && (
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === "ios" ? (
                  "Filter"
                ) : (
                  <IonIcon icon={options} slot="icon-only" />
                )}
              </IonButton>
            )}
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen={true}>
        <IonCard className="speaker-card">
          <IonCardHeader>
            <IonItem
              button
              detail={false}
              lines="none"
              className="speaker-item"
            >
              <IonLabel></IonLabel>
            </IonItem>
          </IonCardHeader>
            {
              produits !== null && produits !== undefined ? 
              <IonCardContent>
                <IonList lines="none">
                  <IonLabel>
                    <IonItem>
                      <h1>{produits.designation}</h1>
                    </IonItem>
                  </IonLabel>
                  <IonLabel>
                    <IonItem>
                    <img className="image"  src="assets/img/appicon.svg" alt="Ionic logo" />
                    
                    </IonItem>
                   
                  </IonLabel>
                 
                  
                  <IonItem >
                    
                  <IonLabel>
                      <h3>{produits.designation}</h3>
                      </IonLabel>
                  </IonItem>
                  <IonItem>
                      <IonButton onClick={() => {
                        sessionStorage.setItem('produit',JSON.stringify(produits) );
                        window.location.replace('/AjoutEnchere');
                      }}>Mettre aux encheres</IonButton>
                  </IonItem>
                  <IonItem>
                      <h4>Categorie</h4>
                      <p>{produits.categorie.designation}</p>
                  </IonItem>
                  <IonItem>
                      <h4>Prix Minimum</h4>
                      <p>{produits.prixmin}</p>
                  </IonItem>
                  <IonItem>
                      <IonTextarea rows={5} value={produits.description} autoGrow={true} readonly />
                  </IonItem>
                </IonList>
              
              
              </IonCardContent> : <></>
            }
        </IonCard>
      </IonContent>
    </IonPage>
  );
};
// let textValue = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum quisquam sed expedita itaque dolores perspiciatis alias aliquid? Iure consectetur atque corrupti, laborum exercitationem nisi ipsum ut mollitia, quod consequatur optio. "


export default connect<OwnProps, StateProps, DispatchProps>({
  mapDispatchToProps: {},
  component: React.memo(SchedulePage),
});
