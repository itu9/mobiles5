import React, { useState, useRef, useEffect } from "react";

import {
  IonToolbar,
  IonContent,
  IonPage,
  IonButtons,
  IonTitle,
  IonMenuButton,
  IonSegment,
  IonSegmentButton,
  IonButton,
  IonIcon,
  IonSearchbar,
  IonHeader,
  getConfig,
  IonGrid,
  IonCol,
  IonRow,
  IonItem,
  IonAvatar,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonLabel,
  IonList,
  IonBackButton,
  IonImg,
  IonTextarea,
  IonInput,
  IonText,
} from "@ionic/react";

import "./Enchere.scss";

import { connect } from "../../../data/connect";

import { logoChrome, options, search } from "ionicons/icons";

import { useParams } from "react-router";
import { SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG } from "constants";
import { Camera, CameraResultType, Photo } from "@capacitor/camera";

interface OwnProps {}

interface StateProps {
  mode: "ios" | "md";
}

interface DispatchProps {}

type AllPageProps = OwnProps & StateProps & DispatchProps;

const SchedulePage: React.FC<AllPageProps> = ({ mode }) => {
  const [segment, setSegment] = useState<"all" | "favorites">("all");
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  const [showCompleteToast, setShowCompleteToast] = useState(false);
  const [image, setImage] = useState<Photo | undefined>(undefined);
  const pageRef = useRef<HTMLElement>(null);
  const [solde,setSolde] = useState<any>();
  const {id} = useParams<{ id: string }>();
  const mise = useRef(null);

  const ios = mode === "ios";

  let token = sessionStorage.getItem("token");
  if(token === null || token === undefined) {
    window.location.replace('/')
    // return <></>;
    token = ''
  }
  const login = JSON.parse(token).login
  useEffect(() => {
    fetch('http://localhost:8080/solde/'+login.id)
    .then(response => response.json())
    .then(response => {
      setSolde(response.data.solde)
    })
  }, [])

  const recharge = () => {
    let data = {
      etat : 0,
      montant : ((mise?.current) as unknown as HTMLInputElement).value,
      login : {
        id : login.id
      }
    }
    fetch('https://limping-quince-production.up.railway.app/recharge',{
      method : 'POST',
      headers : {
        'Content-Type': 'application/json'
      },
      body : JSON.stringify(data)
    }).then(response => response.json())
    .then(response => {
      console.log(response)
      window.location.reload();
    })
    .catch(err => {

    });
  }

  const doRefresh = () => {
    setTimeout(() => {
      ionRefresherRef.current!.complete();
      setShowCompleteToast(true);
    }, 2500);
  };

  const uploadImage = async () => {
    setImage(
      await Camera.getPhoto({
        quality: 90,
        allowEditing: true,
        resultType: CameraResultType.Base64,
      })
    );

    console.log(JSON.stringify(image));
  };

  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/ListeEnchere"></IonBackButton>
          </IonButtons>
          {!showSearchbar && (
            <IonButtons slot="end">
              <IonMenuButton />
            </IonButtons>
          )}

          {!ios && !showSearchbar && <IonTitle>Solde et Rechargement</IonTitle>}

          <IonButtons slot="end">
            {!ios && !showSearchbar && (
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            )}
            {!showSearchbar && (
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === "ios" ? (
                  "Filter"
                ) : (
                  <IonIcon icon={options} slot="icon-only" />
                )}
              </IonButton>
            )}
          </IonButtons>
        </IonToolbar>
      </IonHeader>
<br /><br /><br /><br />
      <IonContent fullscreen={true}>
      <form>
          <IonList>
            <IonItem>
            <IonLabel position="stacked" color="primary">
                <h3> Solde Actuelle</h3>
              </IonLabel>
              <IonLabel position="stacked" color="primary">
              {solde} Ariary                
              </IonLabel>
              
            </IonItem>
<br /><br />
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Recharger
              </IonLabel>
              <IonInput name="solde" type="number" placeholder="12000" ref={mise}></IonInput>
            </IonItem>
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton onClick={recharge} expand="block">
                Valider
              </IonButton>
            </IonCol>
          </IonRow>
        </form>
      </IonContent>
    </IonPage>
  );
};
let textValue = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum quisquam sed expedita itaque dolores perspiciatis alias aliquid? Iure consectetur atque corrupti, laborum exercitationem nisi ipsum ut mollitia, quod consequatur optio. "


export default connect<OwnProps, StateProps, DispatchProps>({
  mapDispatchToProps: {},
  component: React.memo(SchedulePage),
});
