import React, { useState, useRef, useEffect } from "react";

import {
  IonToolbar,
  IonContent,
  IonPage,
  IonButtons,
  IonTitle,
  IonMenuButton,
  IonSegment,
  IonSegmentButton,
  IonButton,
  IonIcon,
  IonSearchbar,
  IonHeader,
  getConfig,
  IonGrid,
  IonCol,
  IonRow,
  IonItem,
  IonAvatar,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonLabel,
  IonList,
  IonBackButton,
  IonImg,
} from "@ionic/react";




import "./Enchere.scss";

import { connect } from "../../../data/connect";

import { options, search } from "ionicons/icons";

import { useParams } from "react-router";
import { SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG } from "constants";
import { Camera, CameraResultType, Photo} from "@capacitor/camera";

interface OwnProps {}

interface StateProps {
  mode: "ios" | "md";
}

interface DispatchProps {

}

type AllPageProps = OwnProps & StateProps & DispatchProps;

const SchedulePage: React.FC<AllPageProps> = ({


  mode,
}) => {
  const [segment, setSegment] = useState<"all" | "favorites">("all");
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  const [showCompleteToast, setShowCompleteToast] = useState(false);
  const [image,setImage] = useState<Photo | undefined>(undefined);
  const pageRef = useRef<HTMLElement>(null);
  const [enchere, setEnchere] = useState<any>([]);


  const ios = mode === "ios";

  const doRefresh = () => {
    setTimeout(() => {
      ionRefresherRef.current!.complete();
      setShowCompleteToast(true);
    }, 2500);
  };

  // const idVoiture = useParams< {id:string } >();



const uploadImage = async() =>{
  setImage(await Camera.getPhoto({
    quality: 90,
    allowEditing: true,
    resultType: CameraResultType.Base64

  }));
  console.log(JSON.stringify(image));
}

  useEffect(() => {
    fetch('https://limping-quince-production.up.railway.app/enchere')
    .then(response => response.json())
    .then(response => {
      console.log(response.data);
      setEnchere(response.data)
    })
  },[])


  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
        <IonButtons slot="start">
            <IonBackButton defaultHref="/ListeEnchere"></IonBackButton>
          </IonButtons>
          {!showSearchbar &&
            <IonButtons slot="end">
              <IonMenuButton />
            </IonButtons>
          }

          {!ios && !showSearchbar &&
            <IonTitle>Liste Enchere</IonTitle>
          }


          <IonButtons slot="end">
            {!ios && !showSearchbar &&
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            }
            {!showSearchbar &&
              <IonButton onClick={() => setShowFilterModal(true)}>
                {mode === 'ios' ? 'Filter' : <IonIcon icon={options} slot="icon-only" />}
              </IonButton>
            }
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen={true}>
        {
          enchere.map((enc:any) => (
            <IonCard key={enc.id} className="speaker-card"routerLink={`/DetailsEnchere/`+enc.id}>
              <IonCardHeader>
                <IonItem button detail={false} lines="none" className="speaker-item">
                  <IonLabel>
                  <h2>{enc.designation}</h2>
                  <br />
                  <img className="imageSmall" src="assets/img/appicon.svg" alt="Ionic logo" />

                  </IonLabel>
                </IonItem>
                <IonItem>
                  <IonLabel>
                    <p>Mise Depart: {enc.minimum}</p>
                    <p>00:00:00</p>
                  </IonLabel>
                </IonItem>

              </IonCardHeader>
            </IonCard>
          ))
        }

      </IonContent>

    </IonPage>
  );
};

export default connect<OwnProps, StateProps, DispatchProps>({
  mapDispatchToProps: {

  },
  component: React.memo(SchedulePage),
});
