import React, { useState } from "react";
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonPage,
  IonButtons,
  IonMenuButton,
  IonRow,
  IonCol,
  IonButton,
  IonList,
  IonItem,
  IonLabel,
  IonInput,
  IonText,
  IonBackButton,
} from "@ionic/react";
import "./Login.scss";

import { connect } from "../../data/connect";
import { RouteComponentProps } from "react-router";

interface OwnProps extends RouteComponentProps {}

interface DispatchProps {}

interface LoginProps extends OwnProps, DispatchProps {}

const Login: React.FC<LoginProps> = ({}) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [formSubmitted, setFormSubmitted] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  return (
    <IonPage id="signup-page">
      <IonHeader>
        <IonToolbar>
        <IonButtons slot="start">
            <IonBackButton defaultHref="/login"></IonBackButton>
          </IonButtons>
          <IonTitle>Inscription</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className="login-logo">
          <img src="assets/img/appicon.svg" alt="Ionic logo" />
        </div>

        <form>
          <IonList>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Nom
              </IonLabel>
              <IonInput name="username" type="text" placeholder="Razafinjatoniary"></IonInput>
            </IonItem>

            {formSubmitted && usernameError && (
              <IonText color="danger">
                <p className="ion-padding-start">Nom Requis </p>
              </IonText>
            )}

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Prenom
              </IonLabel>
              <IonInput name="prenom" type="text" placeholder="Nekena"></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Contact
              </IonLabel>
              <IonInput name="contact" type="tel" placeholder="0341395160"></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary" >
                Email
              </IonLabel>
              <IonInput name="email" type="email" placeholder="majoornekena@gmail.com"></IonInput>
            </IonItem>


            <IonItem>
              <IonLabel position="stacked" color="primary">
                Mot De Passe
              </IonLabel>
              <IonInput name="password" type="password" placeholder="**********"></IonInput>
            </IonItem>

           

            {formSubmitted && passwordError && (
              <IonText color="danger">
                <p className="ion-padding-start">Mot De Passe Requis</p>
              </IonText>
            )}
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton type="submit" expand="block">
                S'inscrire
              </IonButton>
            </IonCol>
          </IonRow>
        </form>
      </IonContent>
    </IonPage>
  );
};

export default connect<OwnProps, {}, DispatchProps>({
  mapDispatchToProps: {},
  component: Login,
});
